from django.contrib import admin
from .models import Employee,PhotoImage
# Register your models here.


admin.site.register(Employee)
admin.site.register(PhotoImage)