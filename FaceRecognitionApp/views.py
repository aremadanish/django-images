from django.shortcuts import render
from django.http import HttpResponse
from .models import Employee,PhotoImage
import cv2
import numpy as np
import face_recognition
import os
from django.views.decorators.csrf import csrf_protect
import arrow


# Create your views here.

main_dir = "C:\\FaceRecognition\\django-images\\media\\"

def hello(requests):
	lt = []
	for img_file in os.listdir("C:\\FaceRecognition\\FaceRecognitionApp\\static\\images\\"):
#		print(img_file)
		if img_file.endswith(".png"):
			#print(img_file)
			lt.append(img_file)
			#print(lt)
	context = {'pic':lt}
	print(type(lt[0]))
	print(context)
	return render(requests,'hello.html',context)

def click(requests):
	dt = arrow.now().format("DDMMYYYY")
	tm = arrow.now().format("HHmm")

	cap = cv2.VideoCapture(0)

	while(True):
	    ret, frame = cap.read()
	    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
	    cv2.imshow('frame',gray)
	    face_locations = face_recognition.face_locations(gray)

	    if face_locations != []:
	    	break
	#p = os.path.join(main_dir,'images\\danish.jpg')
	#print(p)
	#cv2.imwrite(p,frame)
	cap.release()
	cv2.destroyAllWindows()

	if len(face_locations) > 1:
		c = 0
		lt = []
		for location in face_locations:
			top, right, bottom, left = location
			crop = frame[top:bottom,left:right]
			p = str(c)+".png"
	#		cv2.imwrite(p,crop)
			for img_file in os.listdir("C:\\FaceRecognition\\FaceRecognitionApp\\static\\images\\"):
	#			print(img_file)
				if img_file.endswith(".png"):
					#print(img_file)
					lt.append(img_file)
					#print(lt)
				
		context = {'pic':lt}
		print(context)
		return render(requests,'second.html',context)
	else:
		for location in face_locations:
			top, right, bottom, left = location
			crop = frame[top:bottom,left:right]
			p = os.path.join(main_dir,'images\\2.jpg')
			cv2.imwrite(p,crop)
		a = PhotoImage(image=p,imageid=(dt+tm))
		a.save()
		return render(requests,'second.html')

def test(requests):
	img = PhotoImage.objects.all()
	params = {'img':img}
	im=''
	for e in img:
		im = e.image
		print(im)
	i = cv2.imread(os.path.join(main_dir,str(im)))
	#cv2.imshow("image",i)
	#cv2.waitKey(0)
	#cv2.destroyAllWindows()
	load_image = face_recognition.load_image_file(im)
	encoding = face_recognition.face_encodings(load_image)
	print(len(encoding))
	return render(requests,'test.html',params)

def encode(requests,nm):
	print(nm)
	a = PhotoImage.objects.filter(imageid=nm)
	ip=''
	for e in a:
		ip = e.image
		print('e is ',ip)
	load_image = face_recognition.load_image_file('C:\\FaceRecognition\\django-images\\media\\images\\danish.jpg')
	encoding = face_recognition.face_encodings(load_image)
	print(len(encoding))
	return render(requests,'second.html')