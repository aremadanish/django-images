from django.db import models

# Create your models here.

class Employee(models.Model):
	name = models.CharField(max_length=50)
	mobile = models.CharField(max_length=20)
	designation = models.CharField(max_length=50)
	image = models.ImageField(upload_to='images')

	def __str__(self):
		return self.name


class PhotoImage(models.Model):
	image = models.ImageField(upload_to='image')
	imageid = models.CharField(max_length=50)

	def __str__(self):
		return self.imageid

	