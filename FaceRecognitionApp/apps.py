from django.apps import AppConfig


class FacerecognitionappConfig(AppConfig):
    name = 'FaceRecognitionApp'
